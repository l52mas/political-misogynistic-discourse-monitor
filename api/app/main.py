# Mini fastapi example app.
from typing import Optional
from pydantic import BaseModel
from fastapi import FastAPI, Request, Depends, Header, UploadFile, File, Form
from fastapi.responses import HTMLResponse
from functools import lru_cache
from .ml_model import classify_transformer
from .db import init_db
from .security import ApiKeySecurity
from .pages import main_page
import time
import html
import os

__VERSION__ = "0.1.0"
__API_NAME__= "Misogynistic Discourse Monitor Project's API"

class ProcessArguments(BaseModel):
    """Arguments to configure the pre-processing the tweets

    Aguments:
        use_lower               Lower the tweet
        demojize                Replace emojis by aliases (eg _:thumbs_up:_)
        process_urls            Replace URLs by string _$URL$_
        process_mentions        Replace mentions by string _$MENTION$_
        process_hashtags        Replace hashtags by string _$HASHTAG$_
        process_emojis          Replace emojis by string _$EMOJI$_
        process_smileys         Replace smiley by string _$SMILEY$_
        process_numbers         Replace number by string _$NUMBER$_
        process_escaped_chars   Replace escaped chars by string _$ESCAPE_CHAR$_

    """
    model: Optional[str] = None
    use_lower: Optional[bool] = True
    demojize: Optional[bool] = True
    process_urls: Optional[bool] =True
    process_mentions: Optional[bool] = True
    process_hashtags: Optional[bool] =True
    process_emojis: Optional[bool] =False
    process_smileys: Optional[bool] =False
    process_numbers: Optional[bool] =False
    process_escaped_chars: Optional[bool] =False

class TweetArguments(ProcessArguments):
    """Arguments to configure the pre-processing the tweets and pass the tweet. Extends tweet ProcessArguments

    Aguments:
        Tweet              Tweet text
    """
    tweet: Optional[str] = None

class APIKeyArguments(BaseModel):
    """API key as argument

    Aguments:
        api_key     String of the API Key
    """
    api_key: str

def create_app(test_config=None):
    """Creates the FastAPI App"""
    START_TIME = time.time()
    STATUS = "Active"

    # Gets the cofiguration it includes reading the .env file
    from . import config
    @lru_cache()
    def get_settings():
        return config.Settings()

    # Gets the settings and initialized the database (TyniDB)
    settings=get_settings()
    db=init_db(settings.DB_LOCATION, settings.API_KEY_EXPIRATION_LIMIT)

    # Sets the APIKey security, it will be handled asdependecy of the protected calls
    api_key_security=ApiKeySecurity(settings.ADMIN_KEY)

    # create app (main) and view to set to a infix on the URL
    app = FastAPI()
    app_ = FastAPI()

    @app_.get('/', response_class=HTMLResponse)
    def main():
        """Main page to test the API"""
        return main_page

    @app_.get('/api/status')
    def status():
        """Prints status of API"""
        models=os.listdir("/models")
        return {
            'name': __API_NAME__,
            'version': __VERSION__,
            'status': STATUS,
            'models': models,
            'uptime': f"{time.time() - START_TIME:2.3f} segs"
            }


    @app_.post('/api/apikey/new',
            dependencies=[Depends(api_key_security)])
    def api_key_create(never_expires=False):
        """Creates a new API Key but it requieres a valid API Key as an access token
        
        Arguments:
            access_token    Header argument of POST call with the API Key that allows access
            never_expires   JSON param that specifies if API Key expires
        """
        return db.create(never_expires)

    @app_.post('/api/apikey/renew',
            dependencies=[Depends(api_key_security)])
    def api_key_renew(api_key: APIKeyArguments):
        """Renews an API Key but it requieres a valid API Key as an access token
        
        Arguments:
            access_token    Header argument of POST call with the API Key that allows access
            api_key         JSON param that specifies the API Key to renew
        """
        if len(api_key.api_key)>0:
            return db.renew(api_key.api_key)
        else:
            return {"status":"error",
                    "message":"No API key provided"}

    @app_.post('/api/apikey/revoke',
            dependencies=[Depends(api_key_security)])
    def api_key_revoke(api_key: APIKeyArguments):
        """Revokes an API Key but it requieres a valid API Key as an access token
        
        Arguments:
            access_token    Header argument of POST call with the API Key that allows access
            api_key         JSON param that specifies the API Key to renew
        """

        if len(api_key.api_key)>0:
            return db.revoke(api_key.api_key)
        else:
            return {"status":"error",
                    "message":"No API key provided"}

    @app_.post('/api/apikey/stats',
            dependencies=[Depends(api_key_security)])
    def api_key_stats(api_key: APIKeyArguments):
        """Gets statistics of use of an API Key but it requieres a valid API Key as an access token
        
        Arguments:
            access_token    Header argument of POST call with the API Key that allows access
            api_key   JSON param that specifies the API Key to renew
        """
        if len(api_key.api_key)>0:
            return db.stats(api_key.api_key)
        else:
            return {"status":"error",
                    "message":"No API key provided"}

    @app_.post('/api/classify',
            dependencies=[Depends(api_key_security)])
    def post_classify(tweet_args: TweetArguments):
        """Classifies a tweet it need valid API Key as an access token
        
        Arguments:
            TweetArguments  JSON with the tweet arguments
        """
        start_time = time.time()
        elapsed_time = lambda: time.time() - start_time
        tweet=html.unescape(tweet_args.tweet)
        cls, score, tweet_, lang = classify_transformer(
                tweet,
                model=tweet_args.model,
                use_lower=tweet_args.use_lower,
                demojize=tweet_args.demojize,
                process_urls=tweet_args.process_urls,
                process_mentions=tweet_args.process_mentions,
                process_hashtags=tweet_args.process_hashtags,
                process_emojis=tweet_args.process_emojis,
                process_smileys=tweet_args.process_smileys,
                process_numbers=tweet_args.process_numbers,
                process_escaped_chars=tweet_args.process_escaped_chars)
        return {
                "tweet": tweet,
                "tweet_": tweet_,
                "score": score,
                "lang": lang,
                "label": "misogynistic" if cls else "no_evidence",
                "value": cls,
                "elapsed_time": f'{elapsed_time():2.4f} segs'
                }

    @app_.post('/api/classify_file',
            dependencies=[Depends(api_key_security)])
    def post_classify_file(
            use_lower: bool =Form(...),
            model: str = Form(...),
            demojize: bool = Form(...),
            process_urls: bool =Form(...),
            process_mentions: bool =Form(...) ,
            process_hashtags: bool =Form(...),
            process_emojis: bool =Form(...),
            process_smileys: bool =Form(...),
            process_numbers: bool =Form(...),
            process_escaped_chars: bool =Form(...),
            uploaded_file: UploadFile = File(...)):
        """Classifies a file with tweets per line it needs valid API Key as an access token
        
        Arguments:
            TweetArguments  JSON with the tweet arguments

        """
        start_time = time.time()
        elapsed_time = lambda: time.time() - start_time
        results=[]
        if model=='null':
            model=None
        contents= uploaded_file.file.readlines()
        for tweet in contents:
            tweet=tweet.decode('utf-8').strip()
            cls, score, tweet_, lang = classify_transformer(
                tweet,
                model,
                use_lower=use_lower,
                demojize=demojize,
                process_urls=process_urls,
                process_mentions=process_mentions,
                process_hashtags=process_hashtags,
                process_emojis=process_emojis,
                process_smileys=process_smileys,
                process_numbers=process_numbers,
                process_escaped_chars=process_escaped_chars)
            results.append({
                "tweet": tweet,
                "tweet_": tweet_,
                "lang": lang,
                "score": score,
                "label": "misogynistic" if cls else "no_evidence",
                "value": cls,
                })
        return {
                "results" : results,
                "elapsed_time": f'{elapsed_time():2.4f} segs'
                }

    @app_.get('/api/classify',
            dependencies=[Depends(api_key_security)])
    def get_classify(tweet: str = "Esto es un tuit @ivanvladimir",
            model: str = None,
            use_lower: bool = True,
            demojize: bool = True,
            process_urls: bool =True,
            process_mentions: bool = True,
            process_hashtags: bool =True,
            process_emojis: bool =False,
            process_smileys: bool =False,
            process_numbers: bool =False,
            process_escaped_chars: bool =False
            ):
        """Classifies a tweet it need valid API Key as an access token
            
        Arguments:
            use_lower               Lower the tweet
            demojize                Replace emojis by aliases (eg _:thumbs_up:_)
            process_urls            Replace URLs by string _$URL$_
            process_mentions        Replace mentions by string _$MENTION$_
            process_hashtags        Replace hashtags by string _$HASHTAG$_
            process_emojis          Replace emojis by string _$EMOJI$_
            process_smileys         Replace smiley by string _$SMILEY$_
            process_numbers         Replace number by string _$NUMBER$_
            process_escaped_chars   Replace escaped chars by string _$ESCAPE_CHAR$_"""
        start_time = time.time()
        elapsed_time = lambda: time.time() - start_time
        tweet=html.unescape(tweet)
        cls, score, tweet_,lang = classify_transformer(
                tweet,
                model=model,
                use_lower=use_lower,
                demojize=demojize,
                process_urls=process_urls,
                process_mentions=process_mentions,
                process_hashtags=process_hashtags,
                process_emojis=process_emojis,
                process_smileys=process_smileys,
                process_numbers=process_numbers,
                process_escaped_chars=process_escaped_chars)
        return {
                "tweet": tweet,
                "tweet_": tweet_,
                "lang": lang,
                "score": score,
                "label": "misogynistic" if cls else "no_evidence",
                "value": cls,
                "elapsed_time": f'{elapsed_time():2.4f} segs'
                }

    # Creates the infix for the calls
    app.mount("/pmdm", app_)
    return app


app=create_app()
