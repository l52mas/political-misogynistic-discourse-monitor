from pydantic import BaseSettings
import os

# Main configuration for API
class Settings(BaseSettings):
    app_name: str = "Political Misogynistic Discourse Monitor"
    DB_LOCATION: str = "/db/apikeys.db"
    API_KEY_EXPIRATION_LIMIT: int = 1
    ADMIN_KEY: str = ""

    class Config:
        env_file = ".env"
        env_file_encoding = 'utf-8'
