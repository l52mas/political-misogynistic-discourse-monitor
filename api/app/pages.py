main_page="""
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Political Misogynistic Discourse Monitor</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://unpkg.com/vue@3.2.21"></script>
  </head>
  <body>
  <section class="section is-small">
    <div class="container">
      <h1 class="title">
        Classifier for tweets
      </h1>
      <p class="subtitle">
        Political Misogynistic Discourse Monitor
      </p>
    </div>
  </section>

  <section class="section" id="analysis">
    <div class="container">
       <div class="field">
	<label class="label">Access key</label>
    <p>Paste a valid access key<p/>
	  <p class="control">
	    <input v-model="access_token" class="input" type="key" placeholder="Access key">
	  </p>
    </div>

    <div class="control">
    <label for="pt" class="ratio">
    <input type="radio" id="pt" value="pt" v-model="lang">
    Portuguese
    </label>
    <label for="es" class="ratio">
    <input type="radio" id="es" value="es" v-model="lang">
    Spanish
    </label>
    </div>


    <hr/>
 <div class="columns">
    <div class="column is-two-thirds">

    <div class="field">
	<label class="label">Tweet</label>
        <p>Paste a tweet in text area an click "Analyse"<p/>
	  <p class="control">
	    <textarea v-model="tweet" class="textarea" type="tweet" placeholder="Tweet" rows='5'></textarea>
	  </p>
    </div>

    <div class="field is-grouped">
      <div class="control">
            <button v-on:click="analyse_tweet" class="button is-success is-light">Analyse</button>
      </div>
      <div class="control">
            <button v-on:click="clean_tweet" class="button is-danger is-light">Clean</button>
      </div>
      <div class="control">
            <button v-on:click="clean_all" class="button is-danger is-light">Clean all</button>
      </div>
    </div>
    </div>
     <div class="column">

    <article class="message" :class="type">
      <div class="message-header">
	<p>{{label}}</p>
      </div>
      <div class="message-body">
	<p>
	    <table class="table is-striped">
	        <tbody>
                <tr><td><strong>Tweet</strong></td><td>: </td><td>  {{analysed_tweet}}</td></tr>
                <tr><td><strong>Processed</strong></td><td>:  </td><td> {{processed_tweet}}</td></tr>
                <tr><td><strong>Language</strong></td><td>:  </td><td> {{lang_res}}</td></tr>
                <tr><td><strong>Score</strong></td><td>:  </td><td> {{score}}</td></tr>
                <tr><td><strong>Elapsed time</strong></td><td>:  </td><td> {{elapsed_time}}</td></tr>
                </tbody>
	    </table>
	</p>

      </div>
    </article>
      </div>
      </div>

    <hr/>

    <div class="field">
    <label class="label">File</label>
    <p>Select file to analyse, each line must be a tweet<p/>
    <div class="file">
      <label class="file-label">
          <input class="file-input" type="file" name="resume" @change="handleFileChange">
              <span class="file-cta">
                <span class="file-icon">
                  <i class="fas fa-upload"></i>
                </span>
              <span class="file-label">
              Choose a file…
              </span>
          </span>
          <span class="file-name" v-if="filename.length>0">{{filename}}</span>
          <span class="file-name" v-else>Select File</span>
      </label>
    </div>
    </div>

    <div class="field is-grouped">
      <div class="control">
            <button v-on:click="analyse_file" class="button is-success is-light">Analyse</button>
      </div>
      <div class="control">
            <button v-on:click="clean_file" class="button is-danger is-light">Clean</button>
      </div>
      <div class="control">
            <button v-on:click="clean_table" class="button is-danger is-light">Clean all</button>
      </div>
    </div>

  <p v-if="elapsed_time_file">{{elapsed_time_file}}</p>
  <table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Tweet</th>
      <th>Processed</th>
      <th>Language</th>
      <th>Label</th>
      <th>Score</th>
    </tr>
  </thead>

  <tbody>
    <tr v-for="(analysis,i) in results" :key="i">
       <td>{{ i }}</td>
       <td>{{ analysis.tweet }}</td>
       <td>{{ analysis.tweet_ }}</td>
       <td>{{ analysis.lang }}</td>
       <td>
        <span v-if="analysis.value" class="tag is-danger">{{analysis.label}}</span>
        <span v-else class="tag is-success">{{analysis.label}}</span>
       </td>
       <td>{{ analysis.score.toFixed(4) }}</td>
    </tr>
   </tbody>
</table>

  </section>

  <script>
  const ClassifierApp = {
    data() {
            return {
                message: 'You loaded this page on ' + new Date().toLocaleString(),
                tweet: "",
		type: 'is-dark',
                analysed_tweet:"",
                processed_tweet:"",
                label: "No analysis",
                access_token: "",
                score: "",
                lang: null,
                elapsed_time: "",
                filename: "",
		results: [],
            }
        },
    methods:{
        analyse_tweet(){
            var currentUrl = window.location.pathname;
            fetch(currentUrl+'api/classify',{
                method: 'POST',
                body: JSON.stringify({
                    tweet:this.tweet,
                    model:this.lang,
                    use_lower:true,
                    demojize:true,
                    process_urls:true,
                    process_mentions:true,
                    process_hashtags:true,
                    process_emojis:false,
                    process_smileys:false,
                    process_numbers:false,
                    process_escaped_chars:false
                }),
                headers:
                {
                    'accept': 'application/json',
                    'Content-Type': 'application/json',
                    'access-token': this.access_token,
                }
            }).then(response => response.json())
              .then(data => {
                this.analysed_tweet= data.tweet;
                this.processed_tweet= data.tweet_;
                this.label= data.label;
                this.score= data.score;
                this.lang_res=data.lang;
                this.elapsed_time=data.elapsed_time;
                if(data.value){
                    this.type="is-danger";
                }else{
                    this.type="is-success";
                }
                }
              );
        },
        analyse_file(){
            if(this.filename.length>0){
            var currentUrl = window.location.pathname;
            const formData = new FormData();
            formData.append('uploaded_file', this.file);
            formData.append('model',this.lang);
            formData.append('use_lower',true);
            formData.append('demojize',true);
            formData.append('process_urls',true);
            formData.append('process_mentions',true);
            formData.append('process_hashtags',true);
            formData.append('process_emojis',false);
            formData.append('process_smileys',false);
            formData.append('process_numbers',false);
            formData.append('process_escaped_chars',false);
            const myHeaders = new Headers();
            myHeaders.append('accept', 'application/json');
            myHeaders.append('access-token', this.access_token);
            fetch(currentUrl+'api/classify_file',{
                method: 'POST',
                body: formData,
                headers: myHeaders
            }).then(response => response.json())
              .then(data => {
                this.results= data.results;
                this.elapsed_time_file=data.elapsed_time;
                }
              );
        }},

        clean_tweet(){
            this.tweet="";
            },
        clean_file(){
            this.filename="";
            this.file=null;
            this.elapsed_time_file=null;
            },

        clean_table(){
            this.filename="";
            this.file=null;
            this.results=[];
            this.elapsed_time_file=null;
            },


        clean_all(){
            this.tweet="";
            this.type="is-dark"
            this.analysed_tweet="";
            this.processed_tweet="";
            this.score="";
            this.label="No analysis";
            this.elapsed_time=""; 
            },
        handleFileChange(e) {
            console.log(e),
            this.filename=e.target.files[0].name
            this.file=e.target.files[0]
        }
    }
  }

  Vue.createApp(ClassifierApp).mount('#analysis')
  </script>

  </body>
</html>
"""
