# Loads the ML model
import preprocessor as p
import emoji
from transformers import AutoTokenizer, AutoModelForSequenceClassification, pipeline
import gcld3

models={}

lang_detector=gcld3.NNetLanguageIdentifier(min_num_bytes=0, max_num_bytes=8000)

def init_transformer(model,tokenizer=None):
    """ Initializes the transforment model """
    global models

    model_=f"/models/{model}"

    if model in models:
        return models[model]

    if tokenizer is None:
        tokenizer=model

    tokenizer = AutoTokenizer.from_pretrained(model_)
    model__ = AutoModelForSequenceClassification.from_pretrained(model_)

    labeler=pipeline('text-classification', model=model__, tokenizer=tokenizer)
    models[model]=labeler
    if len(models)==1:
        models["__DEFAULT__"]=labeler
    return labeler


def pre_process(text,
        use_lower=False,
        demojize=True,
        process_urls=True,
        process_mentions=True,
        process_hashtags=True,
        process_emojis=False,
        process_smileys=False,
        process_numbers=False,
        process_escaped_chars=False):
    """ Preprocess a tweet """
    opts=[]
    opts.append(p.OPT.URL) if process_urls else None
    opts.append(p.OPT.MENTION) if process_mentions else None
    opts.append(p.OPT.HASHTAG) if process_hashtags else None
    opts.append(p.OPT.EMOJI) if process_emojis else None
    opts.append(p.OPT.SMILEY) if process_smileys else None
    opts.append(p.OPT.NUMBER) if process_numbers else None
    opts.append(p.OPT.ESCAPE_CHAR) if process_escaped_chars else None
    # If transform to lower
    if use_lower:
        text=text.lower()

    if demojize:
        text=emoji.demojize(text)

    p.set_options(*opts)
    text=p.tokenize(text)
    return text

def classify_transformer(
        tweet,
        model=None,
        use_lower=False,
        demojize=True,
        process_urls=True,
        process_mentions=True,
        process_hashtags=True,
        process_emojis=False,
        process_smileys=False,
        process_numbers=False,
        process_escaped_chars=False):
    """Clasifies a twet"""
    global models
    global lang_detector

    tweet=pre_process(tweet, 
                use_lower=use_lower,
                demojize=demojize,
                process_urls=process_urls,
                process_mentions=process_mentions,
                process_hashtags=process_hashtags,
                process_emojis=process_emojis,
                process_smileys=process_smileys,
                process_numbers=process_numbers,
                process_escaped_chars=process_escaped_chars)
    if model is None:
        result = lang_detector.FindLanguage(text=tweet.replace('$MENTION$',""))
        model=result.language
        if not model in ['es','pt']:
            model='pt'
    if not model in models:
        init_transformer(model)
    res=models[model](tweet)[0]

    if res['label'][-1]=="0":
        return False, res['score'], tweet, model
    else:
        return True, res['score'], tweet, model
