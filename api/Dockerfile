# Flask app Dockerfile
FROM huggingface/transformers-pytorch-cpu

WORKDIR /opt/api

# App non-root user
ENV GROUP=app
ENV USER=fastapi
ENV UID=34567
ENV GID=45678
ARG MODEL_NAME
ARG API_FOLDER
ARG APP_MODULE
ENV MODEL_NAME $MODEL_NAME
ENV API_FOLDER $API_FOLDER
ENV APP_MODULE $APP_MODULE
RUN addgroup --gid "$GID" "$GROUP" \
  && adduser --uid "$UID" \
    --disabled-password \
    --gecos "" \
    --ingroup "$GROUP" \
    "$USER"
RUN mkdir /models
RUN chown $USER:$GROUP /models
RUN mkdir /db
RUN chown $USER:$GROUP /db

# Switch to the non-root user
USER "$USER"
ENV PATH="/home/$USER/.local/bin:${PATH}"

# Copy requirements file to our container, install, and remove
# files to we don't need to reduce the container size
COPY requirements.txt .
RUN pip install \
    --no-cache-dir \
    --no-warn-script-location \
    --user \
    -r requirements.txt \
  && find "/home/$USER/.local" \
    \( -type d -a -name test -o -name tests \) \
    -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
    -exec rm -rf '{}' +

# Copy api to container (with privileges to non-root user)
COPY --chown=$USER:$GROUP . .
COPY --chown=$USER:$GROUP .env .
# RUN python3 scripts/download_model.py

